<?php

declare (strict_types=1);

use App\Article;
use App\Author;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/** @var Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Article::class, function (Faker $faker) {
    $title = $faker->unique()->sentence();

    return [
        'title' => $title,
        'cover' => 'articles/' . $faker->file(resource_path('img'), storage_path('app/public/articles'), false),
        'description' => $faker->paragraphs(rand(2,10), true),
        'slug' => Str::slug($title),
        'author_id' => function() {
            return factory(Author::class)->create();
        },
        'reference_article_id' => null,
    ];
});
