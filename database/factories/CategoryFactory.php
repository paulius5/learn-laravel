<?php

declare (strict_types=1);

use App\Category;
use Faker\Generator as Faker;

/** @var Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Category::class, function (Faker $faker) {
    $title = $faker->unique()->sentence(3, false);

    return [
        'title' => $title,
        'slug' => str_slug($title),
        'reference_category_id' => null,
    ];
});


$factory->defineAs(Category::class, 'all_categories', function (){
    return [
        'title' => 'Visos naujienos',
        'slug' => 'visos-naujienos',
    ];
});