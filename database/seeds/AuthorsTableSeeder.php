<?php

declare (strict_types=1);

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class AuthorsTableSeeder
 */
class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $now = Carbon::now();

        $authors = [
            'Petras Petraitis',
            'Antanas Antanaitis',
            'Jonas Jonaitis',
            'Vardenis Pavardenis',
            'Juozas Juozaitis'
        ];

        $data = [];

        foreach ($authors as $author) {
            list($firsName, $lastName) = explode(' ', $author);

            array_push($data, [
                'first_name' => $firsName,
                'last_name' => $lastName,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }

        DB::table('authors')->insert($data);
    }
}
