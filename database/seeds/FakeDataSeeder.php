<?php

declare (strict_types=1);

use App\Article;
use App\Author;
use App\Category;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

/**
 * Class FakeDataSeeder
 */
class FakeDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 'admin_user')->create();

        //factory(Author::class, 10)->create();

        /** @var Category $categoryAll */
        $categoryAll = factory(Category::class, 'all_categories')->create();

        /** @var Collection|Category[] $categories */
        $categories = factory(Category::class, 10)->create();

        /** @var Collection|Article[] $articles */
        $articles = factory(Article::class, 10)->create();

        $articles->each(function (Article $article) use ($categoryAll, $categories) {
            $catIds = $categories->pluck('id')->random(random_int(1,$categories->count()))->all();

            array_push($catIds, $categoryAll->id);

            $article->categories()->attach($catIds);
        });
    }
}