<?php

declare (strict_types=1);

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class UsersTableSeeder
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $now = Carbon::now();

        DB::table('users')->insert([
           'name' => 'Admin',
           'email' => 'acuto@inbox.lt',
           'password' => bcrypt('adminas'),
           'created_at' => $now->format('Y-m-d H:i:s'),
           'updated_at' => $now,
        ]);
    }
}
