<?php

declare (strict_types=1);

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class CategoriesTableSeeder
 */
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $now = Carbon::now();

        $languages = ['Visos', 'PHP', 'JavaScript', 'HTML5', 'C#', 'Python'];

        $data = [];

        foreach ($languages as $language) {
            array_push($data, [
                'title' => $language,
                'slug' => Str::slug($language),
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }

        DB::table('categories')->insert($data);
    }
}
