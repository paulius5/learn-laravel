<?php

declare(strict_types = 1);

namespace App\Enum;

/**
 * Class AuthorLocationTypeEnum
 * @package App\Enum
 */
class AuthorLocationTypeEnum extends Enumerable
{
    /**
     * @return AuthorLocationTypeEnum|Enumerable
     * @throws \ReflectionException
     */
    final public static function local(): AuthorLocationTypeEnum
    {
        return self::make('local', 'Local area', 'Lives nearby.');
    }

    /**
     * @return AuthorLocationTypeEnum|Enumerable
     * @throws \ReflectionException
     */
    final public static function europe(): AuthorLocationTypeEnum
    {
        return self::make('europe', 'Europe continent', '');
    }

    /**
     * @return AuthorLocationTypeEnum|Enumerable
     * @throws \ReflectionException
     */
    final public static function asia(): AuthorLocationTypeEnum
    {
        return self::make('asia', 'Asia continent', 'Lives in Asia.');
    }

    /**
     * @return AuthorLocationTypeEnum|Enumerable
     * @throws \ReflectionException
     */
    final public static function africa(): AuthorLocationTypeEnum
    {
        return self::make('africa', 'Africa continent', 'Lives in Africa.');
    }

    /**
     * @return AuthorLocationTypeEnum|Enumerable
     * @throws \ReflectionException
     */
    final public static function australia(): AuthorLocationTypeEnum
    {
        return self::make('australia', 'Australia continent', 'Lives in Australia.');
    }

    /**
     * @return AuthorLocationTypeEnum|Enumerable
     * @throws \ReflectionException
     */
    final public static function n_america(): AuthorLocationTypeEnum
    {
        return self::make('n_america', 'North America continent', 'Lives in North America.');
    }

    /**
     * @return AuthorLocationTypeEnum|Enumerable
     * @throws \ReflectionException
     */
    final public static function s_america(): AuthorLocationTypeEnum
    {
        return self::make('s_america', 'South America continent', 'Lives in South America.');
    }
}