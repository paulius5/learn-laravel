<?php

declare(strict_types = 1);

namespace App\Services\API;

use App\DTO\ArticleDTO;
use App\DTO\ArticleFullDTO;
use App\DTO\ArticlesDTO;
use App\DTO\AuthorDTO;
use App\DTO\CategoriesDTO;
use App\DTO\CategoryDTO;
use App\DTO\PaginatorDTO;
use App\Exceptions\ArticleException;
use App\Article;
use App\Repositories\ArticleRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class ArticleService
 * @package App\Services\API
 */
class ArticleService
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * ArticleService constructor.
     * @param $articleRepository
     */
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * @return PaginatorDTO
     * @throws \App\Exceptions\ApiDataException
     * @throws \Exception
     */
    public function getPaginateData(): PaginatorDTO
    {
        /** @var LengthAwarePaginator $articles */
        $articles = $this->articleRepository->paginate();
        if ($articles->isEmpty()) {
            throw ArticleException::noData();
        }

        $articlesDTO = new ArticlesDTO();

        foreach ($articles as $article) {
            $articlesDTO->setArticle(
                (new ArticleDTO())
                    ->setArticleId($article->id)
                    ->setTitle($article->title)
                    ->setSlug($article->slug)
                    ->setDescription($article->description)
            );
        }

        return new PaginatorDTO(
            $articles->currentPage(),
            collect($articlesDTO),
            $articles->lastPage(),
            $articles->total(),
            $articles->perPage(),
            $articles->nextPageUrl(),
            $articles->previousPageUrl()
        );
    }

    /**
     * @return PaginatorDTO
     * @throws \App\Exceptions\ApiDataException
     * @throws \Exception
     */
    public function getFullData(): PaginatorDTO
    {
        /** @var LengthAwarePaginator $articles */
        $articles = $this->articleRepository->getFullData();
        if ($articles->isEmpty()) {
            throw ArticleException::noData();
        }

        $articlesDTO = new ArticlesDTO();

        foreach ($articles as $article) {
            $categoriesDTO = new CategoriesDTO();
            foreach ($article->categories as $category) {
                $categoriesDTO->setCategory(
                    (new CategoryDTO())
                        ->setCategoryId($category->id)
                        ->setTitle($category->title)
                        ->setSlug($category->slug)
                );
            }
            $authorDTO = (new AuthorDTO())
                ->setAuthorId($article->author->id)
                ->setFirstName($article->author->first_name)
                ->setLastName($article->author->last_name);
            $articleFullDTO = new ArticleFullDTO(
                (new ArticleDTO())
                    ->setArticleId($article->id)
                    ->setTitle($article->title)
                    ->setSlug($article->slug)
                    ->setDescription($article->description),
                $authorDTO,
                collect($categoriesDTO)->get('data')
            );
            $articlesDTO->setArticle($articleFullDTO);
        }

        return new PaginatorDTO(
            $articles->currentPage(),
            collect($articlesDTO),
            $articles->lastPage(),
            $articles->total(),
            $articles->perPage(),
            $articles->nextPageUrl(),
            $articles->previousPageUrl()
        );
    }

    /**
     * @param int $article_id
     * @return ArticleFullDTO
     * @throws \Exception
     */
    public function getFullById(int $article_id): ArticleFullDTO
    {
        /** @var LengthAwarePaginator $articles */
        $article = $this->articleRepository->getFullDataById($article_id);

        $categoriesDTO = new CategoriesDTO();
        foreach ($article->categories as $category) {
            $categoriesDTO->setCategory(
                (new CategoryDTO())
                    ->setCategoryId($category->id)
                    ->setTitle($category->title)
                    ->setSlug($category->slug)
            );
        }

        $authorDTO = (new AuthorDTO())
            ->setAuthorId($article->author->id)
            ->setFirstName($article->author->first_name)
            ->setLastName($article->author->last_name);

        $articleDTO = (new ArticleDTO())
            ->setArticleId($article->id)
            ->setTitle($article->title)
            ->setSlug($article->slug)
            ->setDescription($article->description);

        return new ArticleFullDTO(
            $articleDTO, $authorDTO, collect($categoriesDTO)
        );
    }

    /**
     * @param int $articleId
     * @return ArticleDTO
     * @throws \Exception
     */
    public function getById(int $articleId): ArticleDTO
    {
        /** @var Article $article */
        $article = $this->articleRepository->findOrFail($articleId);
        $dto = new ArticleDTO();

        return $dto
            ->setArticleId($article->id)
            ->setTitle($article->title)
            ->setSlug($article->slug)
            ->setDescription($article->description);
    }
}