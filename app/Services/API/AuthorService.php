<?php

declare(strict_types = 1);

namespace App\Services\API;

use App\Author;
use App\DTO\AuthorDTO;
use App\Exceptions\AuthorException;
use App\Repositories\AuthorRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class AuthorService
 * @package App\Services\API
 */
class AuthorService
{
    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    /**
     * AuthorService constructor.
     * @param $authorRepository
     */
    public function __construct(AuthorRepository $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    /**
     * @return LengthAwarePaginator
     * @throws \App\Exceptions\ApiDataException
     * @throws \Exception
     */
    public function getPaginateData(): LengthAwarePaginator
    {
        /** @var LengthAwarePaginator $authors */
        $authors = $this->authorRepository->paginate();

        if ($authors->isEmpty()) {
            throw AuthorException::noData();
        }

        return $authors;
    }

    /**
     * @param int $authorId
     * @return AuthorDTO
     * @throws \Exception
     */
    public function getById(int $authorId): AuthorDTO
    {
        /** @var Author $author */
        $author = $this->authorRepository->findOrFail($authorId);

        $dto = new AuthorDTO();

        return $dto
            ->setAuthorId($author->id)
            ->setFirstName($author->first_name)
            ->setLastName($author->last_name);
    }
}