<?php

declare(strict_types = 1);

namespace App\Services\API;

use App\Category;
use App\DTO\CategoryDTO;
use App\Exceptions\CategoryException;
use App\Repositories\CategoryRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class CategoryService
 * @package App\Services\API
 */
class CategoryService
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * CategoryService constructor.
     * @param $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return LengthAwarePaginator
     * @throws \App\Exceptions\ApiDataException
     * @throws \Exception
     */
    public function getPaginateData(): LengthAwarePaginator
    {
        /** @var LengthAwarePaginator $categories */
        $categories = $this->categoryRepository->paginate();
        if ($categories->isEmpty()) {
            throw CategoryException::noData();
        }
        return $categories;
    }

    /**
     * @param int $categoryId
     * @return CategoryDTO
     * @throws \App\Exceptions\ApiDataException
     * @throws \Exception
     */
    public function getById(int $categoryId): CategoryDTO
    {
        /** @var CategoryRepository|Category $category */
        $category = $this->categoryRepository->findOrFail($categoryId);
        if (is_null($category)) {
            throw CategoryException::noData();
        }
        $dto = new CategoryDTO();

        return $dto
            ->setCategoryId($category->id)
            ->setTitle($category->title)
            ->setSlug($category->slug);
    }
}