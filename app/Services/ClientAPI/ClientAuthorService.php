<?php

declare (strict_types=1);

namespace App\Services\ClientAPI;

use App\Author;
use App\Repositories\AuthorRepository;
use stdClass;

/**
 * Class ClientAuthorService
 * @package App\Services\ClientAPI
 */
class ClientAuthorService
{
    /**
     * @param stdClass $data
     * @return Author
     * @throws \Exception
     */
    public function saveAuthorFromObject(stdClass $data): Author
    {
        return app(AuthorRepository::class)->updateOrCreate(
            [
                'first_name' => $data->first_name,
                'last_name' => $data->last_name
            ],
            ['reference_author_id' => $data->author_id]
        );
    }
}