<?php

declare (strict_types=1);

namespace App\Repositories;

use App\Article;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticleRepository
 * @package App\Repositories
 */
class ArticleRepository extends Repository
{

    /**
     * @return string
     */
    public function model(): string
    {
        return Article::class;
    }

    /**
     * Return first row from DB or null ir not found
     *
     * @param string $slug
     *
     * @return Builder|\Illuminate\Database\Eloquent\Model|object
     * @throws \Exception
     */
    public function getBySlug(string $slug)
    {
        return $this->getBySlugBuilder($slug)->first();
    }

    /**
     * @param string $slug
     * @param int $id
     * @return Builder|\Illuminate\Database\Eloquent\Model|null|object
     * @throws \Exception
     */
    public function getBySlugAndNotId(string $slug, int $id)
    {
        return $this->getBySlugBuilder($slug)
            ->where('id', '!=', $id)
            ->first();
    }

    /**
     * @return LengthAwarePaginator
     * @throws \Exception
     */
    public function getFullData(): LengthAwarePaginator
    {
        return $this->makeQuery()
            ->with(['author', 'categories'])
            ->paginate();
    }

    /**
     * @param int $articleId
     * @return Article|Model
     * @throws \Exception
     */
    public function getFullDataById(int $articleId): Article
    {
        $article = $this->makeQuery()
            ->with(['author', 'categories'])
            ->findOrFail($articleId);

        return $article;
    }
    /**
     * @param $slug
     * @return Builder
     * @throws \Exception
     */
    private function getBySlugBuilder($slug): Builder
    {
        return $this->makeQuery()
            ->where('slug', $slug);
    }
}