<?php

declare (strict_types=1);

namespace App\Repositories;

use App\Author;

/**
 * Class AuthorRepository
 * @package App\Repositories
 */
class AuthorRepository extends Repository
{
    /**
     * @return string
     */

    public function model(): string
    {
        return Author::class;
    }
}