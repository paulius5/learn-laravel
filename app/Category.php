<?php

declare (strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Category
 *
 * @package App
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $title
 * @property string $slug
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $reference_category_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereReferenceCategoryId($value)
 * @method static updateOrCreate(array $array, array $array1)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Article[] $articles
 */
class Category extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'reference_category_id',
    ];

    /**
     * @return BelongsToMany
     */
    public function articles(): BelongsToMany
    {
        return $this->belongsToMany(Article::class);
    }
}
