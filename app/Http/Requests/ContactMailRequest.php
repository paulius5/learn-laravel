<?php

declare (strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ContactMailRequest
 * @package App\Http\Requests
 */
class ContactMailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'full_name' => 'required|min:3|max:100',
            'email' => 'required|email',
            'content' => 'required',
        ];
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->input('full_name');
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->input('email');
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->input('content');
    }
}
