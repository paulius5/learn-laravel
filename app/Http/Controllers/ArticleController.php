<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Article;
use App\Enum\ArticleTypeEnum;
use App\Http\Requests\ArticleStoreRequest;
use App\Http\Requests\ArticleUpdateRequest;
use App\Repositories\ArticleRepository;
use App\Repositories\AuthorRepository;
use App\Repositories\CategoryRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class ArticleController
 * @package App\Http\Controllers
 */
class ArticleController extends Controller
{
    const COVER_DIRECTORY = 'articles';

    /**
     * @var AuthorRepository
     */
    private $authorRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * ArticleController constructor.
     * @param AuthorRepository $authorRepository
     * @param CategoryRepository $categoryRepository
     * @param ArticleRepository $articleRepository
     */
    public function __construct(AuthorRepository $authorRepository, CategoryRepository $categoryRepository, ArticleRepository $articleRepository)
    {
        $this->middleware('auth');
        $this->authorRepository = $authorRepository;
        $this->categoryRepository = $categoryRepository;
        $this->articleRepository = $articleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {

        /** @var @var LengthAwarePaginator $articles */
        $articles = $this->articleRepository->paginate(3);

        return view('article.list', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     * @throws \Exception
     */
    public function create(): View
    {
        /** @var AuthorRepository $authors */
        $authors = $this->authorRepository->all();

        /** @var CategoryRepository $categories */
        $categories = $this->categoryRepository->all();

        $articleTypes = ArticleTypeEnum::options();

        return view('article.create', compact('authors', 'categories', 'articleTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArticleStoreRequest $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(ArticleStoreRequest $request): RedirectResponse
    {
        $data = [
            'title' => $request->getTitle(),
            'cover' => $request->getCover() ? $request->getCover()->store(self::COVER_DIRECTORY) : null,
            'description' => $request->getDescription(),
            'author_id' => $request->getAuthorID(),
            'slug' => $request->getSlug(),
            'article_type' => $request->getArticleType(),
        ];

        /** @var Article $article */
        $article = $this->articleRepository->create($data);

        $article->categories()->attach($request->getCategoriesIds());

        return redirect()
            ->route('article.index')
            ->with('status', 'Article created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $articleId
     * @return View
     * @throws \Exception
     */
    public function show(int $articleId): View
    {
        $article = $this->articleRepository->find($articleId);

        return view('article.view', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $articleId
     * @return View
     * @throws \Exception
     */
    public function edit(int $articleId): View
    {
        $authors = $this->authorRepository->all();

        $categories = $this->categoryRepository->all();
        $articleTypes = ArticleTypeEnum::options();

        $article = $this->articleRepository->find($articleId);

        return view('article.edit', compact('article', 'authors', 'categories', 'articleTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ArticleUpdateRequest $request
     * @param int $articleId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(ArticleUpdateRequest $request, int $articleId): RedirectResponse
    {
        $data = [
            'title' => $request->getTitle(),
            'description' => $request->getDescription(),
            'author_id' => $request->getAuthorID(),
            'slug' => $request->getSlug(),
            'article_type' => $request->getArticleType(),
        ];

        if ($request->getCover()) {
            $data['cover'] = $request->getCover()->store(self::COVER_DIRECTORY);
        }

        $this->articleRepository->update($data, $articleId);

        /** @var Article $article */
        $article = $this->articleRepository->find($articleId);

        $article->categories()->sync($request->getCategoriesIds());

        return redirect()
            ->route('article.index')
            ->with('status', 'Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $articleId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy($articleId): ?RedirectResponse
    {
        try {
            $this->articleRepository->delete(['id' => $articleId]);

            return redirect()
                ->route('article.index')
                ->with('status', 'Deleted successfully!');
        } catch (\Throwable $exception) {
            return redirect()
                ->route('article.index')
                ->with('error', $exception->getMessage());
        }
    }

}
