<?php

declare (strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Repositories\UserRepository;
use App\Services\UserService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserService $userService
     * @param UserRepository $userRepository
     */
    public function __construct(UserService $userService, UserRepository $userRepository)
    {
        $this->userService = $userService;
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
        $users = $this->userService->getPaginate();

        return view('user.list', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $userId
     * @return View
     * @throws \Exception
     */
    public function edit(int $userId): View
    {
        $user = $this->userRepository->find($userId);

        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $request
     * @param int $userId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(UserUpdateRequest $request, int $userId): RedirectResponse
    {
        $this->userRepository->update([
            'name' => $request->getName(),
            'email' => $request->getEmail(),
        ], $userId);

        return redirect()
            ->route('user.index')
            ->with('status', 'User updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $userId
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $userId): RedirectResponse
    {
        $this->userRepository->delete($userId);

        return redirect()
            ->route('user.index')
            ->with('status', 'User deleted successfully!');
    }
}
