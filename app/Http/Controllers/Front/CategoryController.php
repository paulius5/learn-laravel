<?php

declare (strict_types=1);

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use Illuminate\View\View;

class CategoryController extends Controller
{
    /** @var CategoryRepository */
    private $categoryRepository;

    /**
     * CategoryController constructor.
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param string $categorySlug
     * @return View
     * @throws \Exception
     */
    public function showArticles(string $categorySlug): View
    {
        $category = $this->categoryRepository->getBySlug($categorySlug);

        $articles = $category->articles()->with(['author'])->paginate(4);

        return view('front.category', compact('category', 'articles'));
    }
}
