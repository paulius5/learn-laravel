<?php

declare (strict_types=1);

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\ArticleRepository;
use Illuminate\View\View;

/**
 * Class HomeController
 * @package App\Http\Controllers\Front
 */
class HomeController extends Controller
{
    /** @var ArticleRepository */
    private $articleRepository;

    /**
     * HomeController constructor.
     * @param ArticleRepository $articleRepository
     */
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
        $articles = $this->articleRepository->makeQuery()
            ->with(['author'])
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->limit(4)
            ->get();

        return view('home', compact('articles'));
    }
}
