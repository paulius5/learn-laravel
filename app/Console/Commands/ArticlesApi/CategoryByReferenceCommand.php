<?php

declare(strict_types = 1);

namespace App\Console\Commands\ArticlesApi;

use App\Services\ClientAPI\ClientCategoryService;

/**
 * Class CategoryByReferenceCommand
 * @package App\Console\Commands\ArticlesApi
 */
class CategoryByReferenceCommand extends ArticleBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:category-by-id {reference_category_id : Category ID on 3trd party application}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get category by id';

    /**
     * @var ClientCategoryService
     */
    private $ClientCategoryService;

    /**
     * CategoryByReferenceCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->ClientCategoryService = app()->make(ClientCategoryService::class);
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        try {
            $response = $this->client->get($this->getCallUrl());

            $data = json_decode($response->getBody()->getContents());

            $this->checkSuccess($data);

            $category = $this->ClientCategoryService->saveFromObject($data->data);

            $this->info('Category ' . $category->title . ' updated or created successfully.');
        } catch (\Throwable $exception) {
            $this->error($exception->getMessage());
        }
    }

    /**
     * @return string
     */
    protected function getCallUrl(): string
    {
        return strtr(':url/category/:id', [
            ':url' => parent::getCallUrl(),
            ':id' => $this->argument('reference_category_id'),
        ]);
    }
}