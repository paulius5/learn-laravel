<?php

declare (strict_types=1);

namespace App\DTO;

use App\DTO\Interfaces\ArticleDTOInterface;
use Illuminate\Support\Collection;

/**
 * Class ArticleFullDTO
 * @package App\DTO
 */
class ArticleFullDTO extends BaseDTO implements ArticleDTOInterface
{
    /**
     * @var AuthorDTO
     */
    private $authorDTO;
    /**
     * @var CategoriesDTO
     */
    private $categoriesDTO;

    /**
     * @var
     */
    private $articleDTO;

    /**
     * ArticleFullDTO constructor.
     * @param ArticleDTO $articleDTO
     * @param AuthorDTO $authorDTO
     * @param Collection $categoriesDTO
     */
    public function __construct(ArticleDTO $articleDTO, AuthorDTO $authorDTO, Collection $categoriesDTO)
    {
        $this->articleDTO = $articleDTO;
        $this->authorDTO = $authorDTO;
        $this->categoriesDTO = $categoriesDTO;
    }

    /**
     * @return array
     */
    protected function jsonData(): array
    {
        return [
            'article_id' => $this->articleDTO->getArticleId(),
            'title' => $this->articleDTO->getTitle(),
            'slug' => $this->articleDTO->getSlug(),
            'description' => $this->articleDTO->getDescription(),
            'author' => $this->authorDTO,
            'categories' => $this->categoriesDTO,
        ];
    }
}