<?php
declare (strict_types=1);

namespace App\DTO;

/**
 * Class CategoryDTO
 * @package App\DTO
 */
class CategoryDTO extends BaseDTO
{
    /**
     * @var
     */
    private $categoryId;
    /**
     * @var
     */
    private $title;
    /**
     * @var
     */
    private $slug;

    /**
     * @param int $categoryId
     * @return CategoryDTO
     */
    public function setCategoryId(int $categoryId): CategoryDTO
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * @param string $title
     * @return CategoryDTO
     */
    public function setTitle(string $title): CategoryDTO
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param string $slug
     * @return CategoryDTO
     */
    public function setSlug(string $slug): CategoryDTO
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return int
     */
    private function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @return string
     */
    private function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    private function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return array
     */
    protected function jsonData(): array
    {
        return [
            'category_id' => $this->getCategoryId(),
            'title' => $this->getTitle(),
            'slug' => $this->getSlug(),
        ];
    }
}