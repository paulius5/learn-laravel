<?php

declare (strict_types=1);

namespace App\DTO;

class CategoriesDTO extends BaseDTO
{
    /**
     * @var \Illuminate\Support\Collection
     */
    private $collectionData;

    /**
     * ArticlesDTO constructor.
     */
    public function __construct()
    {
        $this->collectionData = collect();
    }

    /**
     * @param CategoryDTO $categoryDTO
     * @return CategoriesDTO
     */
    public function setCategory(CategoryDTO $categoryDTO): CategoriesDTO
    {
        $this->collectionData->push($categoryDTO);

        return $this;
    }

    /**
     * @return array
     */
    protected function jsonData(): array
    {
        return [
            'data' => $this->collectionData,
        ];
    }
}