<?php

declare(strict_types = 1);

namespace App\Exceptions;
/**
 * Class AuthorException
 * @package App\Exceptions
 */
class AuthorException extends ApiDataException
{

}