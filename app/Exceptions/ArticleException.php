<?php

declare(strict_types = 1);

namespace App\Exceptions;
/**
 * Class ArticleException
 * @package App\Exceptions
 */
class ArticleException extends ApiDataException
{

}