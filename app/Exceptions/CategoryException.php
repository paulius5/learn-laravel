<?php

declare(strict_types = 1);

namespace App\Exceptions;
/**
 * Class CategoryException
 * @package App\Exceptions
 */
class CategoryException extends ApiDataException
{

}