<?php

declare(strict_types = 1);

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Author
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $first_name
 * @property string $last_name
 * @method static Builder|Author whereCreatedAt($value)
 * @method static Builder|Author whereFirstName($value)
 * @method static Builder|Author whereId($value)
 * @method static Builder|Author whereLastName($value)
 * @method static Builder|Author whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $reference_author_id Author id from 3rd party application
 * @method static Builder|Author whereReferenceAuthorId($value)
 * @method static updateOrCreate(array $array, array $array1)
 * @property string $location_type
 * @method static Builder|Author whereLocationType($value)
 */
class Author extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'reference_author_id',
        'location_type',
    ];
}
