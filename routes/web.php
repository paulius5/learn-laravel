<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['namespace' => 'Front'], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('category/{slug}', 'CategoryController@showArticles')->name('front.category.articles');
    Route::get('article/{slug}', 'ArticleController@show')->name('front.article.slug');
    Route::get('articles', 'ArticleController@index')->name('front.articles');
    Route::get('/contacts', 'ContactController@index')->name('contacts');
    Route::post('/contacts', 'ContactController@sendMessage');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'AdminController@index')->name('admin.home');
    Route::resource('article', 'ArticleController');
    Route::resource('author', 'AuthorController')->except(['show', 'destroy']);
    Route::resource('category', 'CategoryController')->except(['show', 'destroy']);
    Route::resource('user', 'UserController')->only(['index', 'edit', 'update', 'destroy']);
});