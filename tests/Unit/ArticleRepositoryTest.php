<?php

declare (strict_types=1);

namespace Tests\Unit;

use App\Article;
use App\Category;
use App\Repositories\ArticleRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Tests\MemoryDatabaseMigrations;
use Tests\TestCase;

/**
 * Class ArticleRepositoryTest
 * @package Tests\Unit
 */
class ArticleRepositoryTest extends TestCase
{
    use MemoryDatabaseMigrations;

    /**
     * @test
     * @group article
     */
    public function it_should_make_singleton_instance(): void
    {
        $this->assertInstanceOf(ArticleRepository::class, $this->getTestClassInstance());

        $this->assertSame($this->getTestClassInstance(), $this->getTestClassInstance());
    }

    /**
     * @test
     * @group article
     *
     * @throws \Exception
     */
    public function it_should_return_null_on_get_by_slug(): void
    {
        $slug = str_random(10);

        $this->assertNull($this->getTestClassInstance()->getBySlug($slug));
    }

    /**
     * @test
     * @group article
     *
     * @throws \Exception
     */
    public function it_should_return_row_on_get_by_slug(): void
    {
        /** @var Article $article */
        $article = factory(Article::class)->create([
            'reference_article_id' => null,
        ]);

        factory(Article::class)->create();

        $result = $this->getTestClassInstance()->getBySlug($article->slug);

        $this->assertInstanceOf(Article::class, $result);

        $this->assertEquals($article->toArray(), $result->toArray());
    }

    /**
     * @test
     * @group article
     *
     * @throws \Exception
     */
    public function it_should_return_null_on_get_by_slug_and_not_id_empty_db(): void
    {
        $slug = str_random(10);

        $id = random_int(1, 10);

        $this->assertNull($this->getTestClassInstance()->getBySlugAndNotId($slug, $id));
    }

    /**
     * @test
     * @group article
     *
     * @throws \Exception
     */
    public function it_should_return_null_on_get_by_slug_and_not_id_not_empty_db(): void
    {
        /** @var Article $article */
        $article = factory(Article::class)->create();

        $this->assertNull($this->getTestClassInstance()->getBySlugAndNotId($article->slug, $article->id));
    }

    /**
     * @test
     * @group article
     *
     * @throws \Exception
     */
    public function it_should_return_row_on_get_by_slug_and_not_id(): void
    {
        /** @var Article $article1 */
        $article1 = factory(Article::class)->create([
            'reference_article_id' => null,
        ]);

        /** @var Article $article2 */
        $article2 = factory(Article::class)->create();

        $result = $this->getTestClassInstance()->getBySlugAndNotId($article1->slug, $article2->id);

        $this->assertInstanceOf(Article::class, $result);

        $this->assertEquals($article1->toArray(), $result->toArray());
    }

    /**
     * @test
     * @group article
     * @group article-repository
     *
     * @throws \Exception
     */
    public function it_should_return_empty_paginator(): void
    {
        $result = $this->getTestClassInstance()->getFullData();

        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertTrue($result->isEmpty());
    }

    /**
     * @test
     * @group article
     * @group article-repository
     * @throws \Exception
     */
    public function it_should_return_paginator_with_data_with_categories(): void
    {
        /** @var Collection|Article[] $articles */
        $articles = factory(Article::class, 15)->create()
            ->each(function (Article $article) {
                $article->categories()->sync(factory(Category::class, 2)->create());
            });

        $expectData = [];

        $articles->each(function (Article $article) use (&$expectData) {
            $item = $article->toArray();

            array_set($item, 'author', $article->author->toArray());
            array_set($item, 'categories', $article->categories->toArray());

            array_push($expectData, $item);
        });

        $result = $this->getTestClassInstance()->getFullData();

        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertTrue($result->isNotEmpty());
        $this->assertEquals($expectData, collect($result->items())->toArray());
    }

    /**
     * @test
     * @group article
     * @group article-repository
     * @throws \Exception
     */
    public function it_should_return_paginator_with_data_without_categories(): void
    {
        /** @var Collection|Article[] $articles */
        $articles = factory(Article::class, 15)->create();

        $expectData = [];

        $articles->each(function (Article $article) use (&$expectData) {
            $item = $article->toArray();

            array_set($item, 'author', $article->author->toArray());
            array_set($item, 'categories', []);

            array_push($expectData, $item);
        });

        $result = $this->getTestClassInstance()->getFullData();

        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        $this->assertTrue($result->isNotEmpty());
        $this->assertEquals($expectData, collect($result->items())->toArray());
    }

    /**
     * @test
     * @group article
     * @group article-repository
     * @throws \Exception
     */
    public function it_should_expect_mode_not_found_on_get_by_id(): void
    {
        $id = mt_rand(1, 10);

        $this->expectException(ModelNotFoundException::class);

        $this->getTestClassInstance()->getFullDataById($id);
    }

    /**
 * @test
 * @group article
 * @group article-repository
 * @throws \Exception
 */
    public function it_should_return_full_data_on_get_full_by_id_without_categories(): void
    {
        /** @var Article $article */
        $article = factory(Article::class)->create();

        $expectData = $article->toArray();
        array_set($expectData, 'author', $article->author->toArray());
        array_set($expectData, 'categories', []);

        $result = $this->getTestClassInstance()->getFullDataById($article->id);

        $this->assertInstanceOf(Article::class, $result);
        $this->assertEquals($expectData, $result->toArray());
    }

    /**
     * @test
     * @group article
     * @group article-repository
     * @throws \Exception
     */
    public function it_should_return_full_data_on_get_full_by_id_with_categories(): void
    {
        /** @var Category[]|Collection $categories */
        $categories = factory(Category::class, 2)->create();
        /** @var Article $article */
        $article = factory(Article::class)->create();
        $article->categories()->sync($categories->pluck('id')->all());

        $expectData = $article->toArray();
        array_set($expectData, 'author', $article->author->toArray());
        array_set($expectData, 'categories', $article->categories->toArray());

        $result = $this->getTestClassInstance()->getFullDataById($article->id);

        $this->assertInstanceOf(Article::class, $result);
        $this->assertEquals($expectData, $result->toArray());
    }

    /**
     * @return ArticleRepository
     */
    private function getTestClassInstance(): ArticleRepository
    {
        return $this->app->make(ArticleRepository::class);
    }
}
