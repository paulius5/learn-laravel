<?php

declare (strict_types=1);

namespace Tests\Unit\Services\API;

use App\Category;
use App\DTO\CategoriesDTO;
use App\DTO\CategoryDTO;
use App\Exceptions\CategoryException;
use App\Repositories\CategoryRepository;
use App\Services\API\CategoryService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Tests\TestCase;

/**
 * Class CategoryServiceTest
 * @package Tests\Unit\Services\API
 */
class CategoryServiceTest extends TestCase
{
    /**
     * @test
     * @group category
     * @group category-service
     */
    public function it_should_create_singleton_instance(): void
    {
        $this->assertInstanceOf(CategoryService::class, $this->getTestClassInstance());
        $this->assertSame($this->getTestClassInstance(), $this->getTestClassInstance());
    }

    /**
     * @test
     * @group category
     * @group category-service
     *
     * @throws \App\Exceptions\ApiDataException
     * @throws \ReflectionException
     */
    public function it_should_fail_on_get_by_id(): void
    {
        $id = rand(1, 10);

        $this->initPHPUnitMock(CategoryRepository::class, null, ['findOrFail'])
            ->expects($this->once())
            ->method('findOrFail')
            ->with($id)
            ->willThrowException(new ModelNotFoundException());

        $this->expectException(ModelNotFoundException::class);

        $this->getTestClassInstance()->getById($id);
    }

    /**
     * @test
     * @group category
     * @group category-service
     *
     * @throws \App\Exceptions\ApiDataException
     * @throws \ReflectionException
     */
    public function it_should_return_category_dto_on_get_by_id(): void
    {
        /** @var Category $category */
        $category = factory(Category::class)->make([
            'id' => mt_rand(1, 10),
        ]);

        $this->initPHPUnitMock(CategoryRepository::class, null, ['findOrFail'])
            ->expects($this->once())
            ->method('findOrFail')
            ->with($category->id)
            ->willReturn($category);

        $expectData = (new CategoryDTO())
            ->setCategoryId($category->id)
            ->setTitle($category->title)
            ->setSlug($category->slug);

        $result = $this->getTestClassInstance()->getById($category->id);

        $this->assertInstanceOf(CategoryDTO::class, $result);

        $this->assertEquals($expectData, $result);
    }

    /**
     * @test
     * @group category
     * @group category-service
     *
     * @throws \App\Exceptions\ApiDataException
     * @throws \ReflectionException
     */
    public function it_should_fail_on_paginate_date(): void
    {
        $this->initPHPUnitMock(CategoryRepository::class, null, ['paginate'])
            ->expects($this->once())
            ->method('paginate')
            ->willReturn(new LengthAwarePaginator(null, 0, 15));

        $this->expectException(CategoryException::class);
        $this->expectExceptionMessage(CategoryException::noData()->getMessage());
        $this->expectExceptionCode(CategoryException::noData()->getCode());

        $this->getTestClassInstance()->getPaginateData();
    }

    /**
     * @test
     * @group category
     * @group category-service
     *
     * @throws \App\Exceptions\ApiDataException
     * @throws \ReflectionException
     */
    public function it_should_get_paginator_dto_data(): void
    {
        $count = 2;
        /** @var Collection|Category[] $categories */
        $categories = factory(Category::class, $count)->make()->each(function (Category $item, $key) {
            $item->id = $key + 1;
        });

        $this->initPHPUnitMock(CategoryRepository::class, null, ['paginate'])
            ->expects($this->once())
            ->method('paginate')
            ->willReturn(new LengthAwarePaginator($categories, $count, 15));

        $expectData = new CategoriesDTO();

        $categories->each(function (Category $category) use (&$expectData) {
            $expectData->setCategory((new CategoryDTO)
                ->setCategoryId($category->id)
                ->setTitle($category->title)
                ->setSlug($category->slug));
        });

        $result = $this->getTestClassInstance()->getPaginateData();

        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
        //$this->assertEquals(collect($expectData)->get('data'), collect($result)->get('data'));
    }

    /**
     * @return CategoryService
     */
    private function getTestClassInstance(): CategoryService
    {
        return $this->app->make(CategoryService::class);
    }
}
