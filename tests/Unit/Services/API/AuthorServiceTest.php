<?php

declare (strict_types=1);

namespace Tests\Unit\Services\API;

use App\Author;
use App\DTO\AuthorDTO;
use App\Exceptions\AuthorException;
use App\Repositories\AuthorRepository;
use App\Services\API\AuthorService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Tests\TestCase;

/**
 * Class AuthorServiceTest
 * @package Tests\Unit\Services\API
 */
class AuthorServiceTest extends TestCase
{
    /**
     * @test
     * @group author
     * @group author-service
     */
    public function it_should_create_singleton_instance(): void
    {
        $this->assertInstanceOf(AuthorService::class, $this->getTestClassInstance());
        $this->assertSame($this->getTestClassInstance(), $this->getTestClassInstance());
    }

    /**
     * @test
     * @group author
     * @group author-service
     *
     * @throws \Exception
     */
    public function it_should_expect_exception(): void
    {
        $id = rand(1, 10);

        $this->initPHPUnitMock(AuthorRepository::class, null, ['findOrFail'])
            ->expects($this->once())
            ->method('findOrFail')
            ->with($id)
            ->willThrowException(new ModelNotFoundException());

        $this->expectException(ModelNotFoundException::class);

        $this->getTestClassInstance()->getById($id);
    }

    /**
     * @test
     * @group author
     * @group author-service
     *
     * @throws \Exception
     */
    public function it_should_return_author_dto_on_get_by_id(): void
    {
        /** @var Author $author */
        $author = factory(Author::class)->make([
            'id' => mt_rand(1, 10),
        ]);

        $this->initPHPUnitMock(AuthorRepository::class, null, ['findOrFail'])
            ->expects($this->once())
            ->method('findOrFail')
            ->with($author->id)
            ->willReturn($author);

        $result = $this->getTestClassInstance()->getById($author->id);

        $expectData = (new AuthorDTO())
            ->setAuthorId($author->id)
            ->setFirstName($author->first_name)
            ->setLastName($author->last_name);

        $this->assertEquals($expectData, $result);
    }

    /**
     * @test
     * @group author
     * @group author-service
     *
     * @throws \App\Exceptions\ApiDataException
     * @throws \ReflectionException
     */
    public function it_should_expect_author_exception(): void
    {
        $this->initPHPUnitMock(AuthorRepository::class, null, ['paginate'])
            ->expects($this->once())
            ->method('paginate')
            ->willReturn(new LengthAwarePaginator(null, 0, 15));

        $this->expectException(AuthorException::class);
        $this->expectExceptionMessage(AuthorException::noData()->getMessage());
        $this->expectExceptionCode(AuthorException::noData()->getCode());

        $this->getTestClassInstance()->getPaginateData();
    }


    /**
     * @test
     * @group author
     * @group author-service
     *
     * @throws \App\Exceptions\ApiDataException
     * @throws \ReflectionException
     */
    public function it_should_return_paginator(): void
    {
        $count = 2;
        /** @var Collection|Author[] $authors */
        $authors = factory(Author::class, $count)->make([
            'id' =>mt_rand(1, 10),
        ]);

        $this->initPHPUnitMock(AuthorRepository::class, null, ['paginate'])
            ->expects($this->once())
            ->method('paginate')
            ->willReturn(new LengthAwarePaginator($authors, $count, 15));

        $result = $this->getTestClassInstance()->getPaginateData();

        $this->assertInstanceOf(LengthAwarePaginator::class, $result);

        $this->assertEquals($authors->toArray(), $result->getCollection()->toArray());
    }

    /**
     * @return AuthorService
     */
    private function getTestClassInstance(): AuthorService
    {
        return $this->app->make(AuthorService::class);
    }
}
