<?php

declare (strict_types=1);

namespace Tests\Unit\Services\API;

use App\Article;
use App\Author;
use App\Category;
use App\DTO\ArticleDTO;
use App\DTO\ArticleFullDTO;
use App\DTO\ArticlesDTO;
use App\DTO\AuthorDTO;
use App\DTO\CategoriesDTO;
use App\DTO\CategoryDTO;
use App\DTO\PaginatorDTO;
use App\Exceptions\ArticleException;
use App\Repositories\ArticleRepository;
use App\Services\API\ArticleService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Tests\BuildsMocks;
use Tests\TestCase;

/**
 * Class ArticleServiceTest
 * @package Tests\Unit\Services\API
 */
class ArticleServiceTest extends TestCase
{
    use BuildsMocks;

    /**
     * @test
     * @group article
     * @group article-service
     */
    public function it_should_make_singleton_instance(): void
    {
        $this->assertInstanceOf(ArticleService::class, $this->getTestClassInstance());
        $this->assertSame($this->getTestClassInstance(), $this->getTestClassInstance());
    }

    /**
     * @test
     * @group article
     * @group article-service
     *
     * @throws \App\Exceptions\ApiDataException
     * @throws \ReflectionException
     */
    public function it_should_expect_exception_on_get_paginate_data(): void
    {
        $this->initPHPUnitMock(ArticleRepository::class, null, ['paginate'])
            ->expects($this->once())
            ->method('paginate')
            ->willReturn(new LengthAwarePaginator(null, 0, 15));

        $this->expectException(ArticleException::class);
        $this->expectExceptionMessage(ArticleException::noData()->getMessage());
        $this->expectExceptionCode(ArticleException::noData()->getCode());

        $this->getTestClassInstance()->getPaginateData();
    }

    /**
     * @test
     * @group article
     * @group article-service
     *
     * @throws \App\Exceptions\ApiDataException
     * @throws \ReflectionException
     */
    public function it_should_return_paginator_dto_on_get_paginate_data(): void
    {
        $count = 2;
        /** @var Collection|Article[] $articles */
        $articles = factory(Article::class, $count)->make([
            'author_id' => mt_rand(1, 10),
        ])
            ->each(function (Article $item, $key) {
                $item->id = $key + 1;
            });

        $this->initPHPUnitMock(ArticleRepository::class, null, ['paginate'])
            ->expects($this->once())
            ->method('paginate')
            ->willReturn(new LengthAwarePaginator($articles, $count, 15));

        $expectData = new ArticlesDTO();

        $articles->each(function (Article $article) use (&$expectData) {
            $expectData->setArticle(
                (new ArticleDTO())
                    ->setArticleId($article->id)
                    ->setTitle($article->title)
                    ->setSlug($article->slug)
                    ->setDescription($article->description)
            );
        });

        $result = $this->getTestClassInstance()->getPaginateData();

        $this->assertInstanceOf(PaginatorDTO::class, $result);

        $this->assertEquals(
            collect($expectData)->get('data'),
            collect($result)->get('articles')
        );
    }

    /**
     * @test
     * @group article
     * @group article-service
     *
     * @throws \App\Exceptions\ApiDataException
     * @throws \ReflectionException
     */
    public function it_should_expect_exception_on_get_full_data(): void
    {
        $this->initPHPUnitMock(ArticleRepository::class, null, ['getFullData'])
            ->expects($this->once())
            ->method('getFullData')
            ->willReturn(new LengthAwarePaginator(null, 0, 15));


        $this->expectException(ArticleException::class);
        $this->expectExceptionMessage(ArticleException::noData()->getMessage());
        $this->expectExceptionCode(ArticleException::noData()->getCode());

        $this->getTestClassInstance()->getFullData();
    }

    /**
     * @test
     * @group article
     * @group article-service
     *
     * @throws \App\Exceptions\ApiDataException
     * @throws \ReflectionException
     */
    public function it_should_return_paginator_dto_on_get_full_data_without_categories(): void
    {
        /** @var Author $author */
        $author = factory(Author::class)->make([
            'id' => mt_rand(1, 10),
        ]);

        /** @var Collection|Article[] $articles */
        $articles = factory(Article::class, 2)->make([
            'author_id' => $author->id,
        ])->each(function (Article $article, $key) {
            $article->id = $key + 1;
        });

        $mockData = [];

        $articles->each(function (Article $article) use (&$mockData, $author) {

            $item = $article->toArray();

            array_set($item, 'author', (object)$author->toArray());

            array_set($item, 'categories', collect());

            array_push($mockData, (object)$item);
        });

        $this->initPHPUnitMock(ArticleRepository::class, null, ['getFullData'])
            ->expects($this->once())
            ->method('getFullData')
            ->willReturn(new LengthAwarePaginator($mockData, 2, 15));

        $expectData = new ArticlesDTO();

        $articles->each(function (Article $article) use (&$expectData, $author) {
            $articleDTO = (new ArticleDTO())
                ->setArticleId($article->id)
                ->setTitle($article->title)
                ->setSlug($article->slug)
                ->setDescription($article->description);

            $authorDTO = (new AuthorDTO())
                ->setAuthorId($author->id)
                ->setFirstName($author->first_name)
                ->setLastName($author->last_name);

            $categoriesDTO = new CategoriesDTO();

            $expectData->setArticle(
                new ArticleFullDTO(
                    $articleDTO,
                    $authorDTO,
                    collect($categoriesDTO)->get('data')
                )
            );
        });

        $result = $this->getTestClassInstance()->getFullData();

        $this->assertInstanceOf(PaginatorDTO::class, $result);

        $this->assertEquals(
            collect($expectData)->get('data'),
            collect($result)->get('articles')
        );
    }

    /**
     * @test
     * @group article
     * @group article-service
     *
     * @throws \App\Exceptions\ApiDataException
     * @throws \ReflectionException
     */
    public function it_should_return_paginator_dto_on_get_full_data_with_categories(): void
    {
        /** @var Author $author */
        $author = factory(Author::class)->make([
            'id' => mt_rand(1, 10),
        ]);
        /** @var Category[]|Collection $categories */
        $categories = factory(Category::class, 2)->make()
            ->each(function (Category $category, $key) {
                $category->id = $key + 1;
            });

        /** @var Collection|Article[] $articles */
        $articles = factory(Article::class, 2)->make([
            'author_id' => $author->id,
        ])->each(function (Article $article, $key) {
            $article->id = $key + 1;
        });

        $mockData = [];

        $articles->each(function (Article $article) use (&$mockData, $author, $categories) {

            $item = $article->toArray();

            array_set($item, 'author', (object)$author->toArray());

            $cats = collect();

            $categories->each(function (Category $category) use ($cats) {
                $cats->push((object)$category->toArray());
            });

            array_set($item, 'categories', $cats);

            array_push($mockData, (object)$item);
        });

        $this->initPHPUnitMock(ArticleRepository::class, null, ['getFullData'])
            ->expects($this->once())
            ->method('getFullData')
            ->willReturn(new LengthAwarePaginator($mockData, 2, 15));

        $expectData = new ArticlesDTO();

        $articles->each(function (Article $article) use (&$expectData, $author, $categories) {
            $articleDTO = (new ArticleDTO())
                ->setArticleId($article->id)
                ->setTitle($article->title)
                ->setSlug($article->slug)
                ->setDescription($article->description);

            $authorDTO = (new AuthorDTO())
                ->setAuthorId($author->id)
                ->setFirstName($author->first_name)
                ->setLastName($author->last_name);

            $categoriesDTO = new CategoriesDTO();

            $categories->each(function (Category $category) use ($categoriesDTO) {
                $categoriesDTO->setCategory((new CategoryDTO())
                    ->setCategoryId($category->id)
                    ->setTitle($category->title)
                    ->setSlug($category->slug));
            });

            $expectData->setArticle(
                new ArticleFullDTO(
                    $articleDTO,
                    $authorDTO,
                    collect($categoriesDTO)->get('data')
                )
            );
        });

        $result = $this->getTestClassInstance()->getFullData();

        $this->assertInstanceOf(PaginatorDTO::class, $result);

        $this->assertEquals(
            collect($expectData)->get('data'),
            collect($result)->get('articles')
        );
    }

    /**
     * @test
     * @group article
     * @group article-service
     *
     * @throws \Exception
     */
    public function it_should_expect_exception_on_get_full_by_id(): void
    {
        $id = rand(1, 10);

        $this->initPHPUnitMock(ArticleRepository::class, null, ['getFullDataById'])
            ->expects($this->once())
            ->method('getFullDataById')
            ->with($id)
            ->willThrowException(new ModelNotFoundException());

        $this->expectException(ModelNotFoundException::class);

        $this->getTestClassInstance()->getFullById($id);
    }

    /**
     * @test
     * @group article
     * @group article-service
     *
     * @throws \Exception
     */
    public function it_should_return_article_full_dto_on_get_full_by_id_without_categories(): void
    {
        $author = factory(Author::class)->make([
            'id' => mt_rand(1, 10),
        ]);

        /** @var Article $article */
        $article = factory(Article::class)->make([
            'id' => mt_rand(1, 10),
            'author_id' => $author->id,
        ]);

        $this->initPHPUnitMock(ArticleRepository::class, null, ['getFullDataById'])
            ->expects($this->once())
            ->method('getFullDataById')
            ->with($article->id)
            ->willReturn($article);

        $article->setAttribute('author', $author);
        $article->setAttribute('categories', collect());

        $categoriesDTO = new CategoriesDTO();

        $expectData = new ArticleFullDTO(
            (new ArticleDTO())
                ->setArticleId($article->id)
                ->setTitle($article->title)
                ->setSlug($article->slug)
                ->setDescription($article->description),
            (new AuthorDTO())
                ->setAuthorId($author->id)
                ->setFirstName($author->first_name)
                ->setLastName($author->last_name),
            collect($categoriesDTO)
        );

        $result = $this->getTestClassInstance()->getFullById($article->id);

        $this->assertInstanceOf(ArticleFullDTO::class, $result);

        $this->assertEquals($expectData, $result);
    }

    /**
     * @test
     * @group article
     * @group article-service1
     *
     * @throws \Exception
     */
    public function it_should_return_article_full_dto_on_get_full_by_id_with_categories(): void
    {
        /** @var Author $author */
        $author = factory(Author::class)->make([
            'id' => mt_rand(1, 10),
        ]);

        /** @var Category[]|Collection $categories */
        $categories = factory(Category::class, 3)->make()
            ->each(function (Category $category, $key) {
                $category->id = $key + 1;
            });

        /** @var Collection|Article $article */
        $article = factory(Article::class)->make([
            'id' => mt_rand(1, 10),
            'author_id' => $author->id,
        ]);

        $article->setAttribute('author', $author);
        $article->setAttribute('categories', $categories);

        $this->initPHPUnitMock(ArticleRepository::class, null, ['getFullDataById'])
            ->expects($this->once())
            ->method('getFullDataById')
            ->with($article->id)
            ->willReturn($article);

        $categoriesDTO = new CategoriesDTO();

        $categories->each(function (Category $category) use ($categoriesDTO) {
            $categoriesDTO->setCategory(
                (new CategoryDTO())
                    ->setCategoryId($category->id)
                    ->setTitle($category->title)
                    ->setSlug($category->slug)
            );
        });

        $expectData = new ArticleFullDTO(
            (new ArticleDTO())
                ->setArticleId($article->id)
                ->setTitle($article->title)
                ->setSlug($article->slug)
                ->setDescription($article->description),
            (new AuthorDTO())
                ->setAuthorId($author->id)
                ->setFirstName($author->first_name)
                ->setLastName($author->last_name),
            collect($categoriesDTO)
        );

        $result = $this->getTestClassInstance()->getFullById($article->id);

        $this->assertInstanceOf(ArticleFullDTO::class, $result);

        $this->assertEquals($expectData, $result);
    }

    /**
     * @test
     * @group article
     * @group article-service
     *
     * @throws \Exception
     */
    public function it_should_expect_exception_on_get_by_id(): void
    {
        $id = rand(1, 10);

        $this->initPHPUnitMock(ArticleRepository::class, null, ['findOrFail'])
            ->expects($this->once())
            ->method('findOrFail')
            ->with($id)
            ->willThrowException(new ModelNotFoundException());

        $this->expectException(ModelNotFoundException::class);

        $this->getTestClassInstance()->getById($id);
    }

    /**
     * @test
     * @group article
     * @group article-service
     *
     * @throws \Exception
     */
    public function it_should_return_article_dto_on_get_by_id(): void
    {
        /** @var Article $article */
        $article = factory(Article::class)->make([
            'id' => mt_rand(1, 10),
            'author_id' => mt_rand(1, 10),
        ]);

        $this->initPHPUnitMock(ArticleRepository::class, null, ['findOrFail'])
            ->expects($this->once())
            ->method('findOrFail')
            ->with($article->id)
            ->willReturn($article);

        $expectData = (new ArticleDTO())
            ->setArticleId($article->id)
            ->setTitle($article->title)
            ->setSlug($article->slug)
            ->setDescription($article->description);

        $result = $this->getTestClassInstance()->getById($article->id);

        $this->assertInstanceOf(ArticleDTO::class, $result);

        $this->assertEquals($expectData, $result);
    }

    /**
     * @return ArticleService
     */
    private function getTestClassInstance(): ArticleService
    {
        return $this->app->make(ArticleService::class);
    }
}
