<?php

declare (strict_types=1);

namespace Tests\Unit;

use App\Repositories\AuthorRepository;
use Tests\TestCase;

/**
 * Class AuthorRepositoryTest
 * @package Tests\Unit
 */
class AuthorRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_make_singleton_instance(): void
    {
        $this->assertInstanceOf(AuthorRepository::class, $this->getTestClassInstance());

        $this->assertSame($this->getTestClassInstance(), $this->getTestClassInstance());
    }

    /**
     * @return AuthorRepository
     */
    private function getTestClassInstance(): AuthorRepository
    {
        return $this->app->make(AuthorRepository::class);
    }
}
