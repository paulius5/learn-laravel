@extends('layouts.front')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="row">
                    @foreach($articles as $article)
                        @include('front.partials._article')
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
