@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Article Edit
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                       <form action="{{ route('article.update', [$article->id]) }}" method="post" enctype="multipart/form-data">

                           @method('put')
                           @csrf

                           <div class="form-group">
                               <label for="title">{{ __('Title') }}:</label>
                               <input id="title" class="form-control" type="text" name="title" value="{{ old('title', $article->title) }}">
                               @if($errors->has('title'))
                                   <div class="alert-danger">{{ $errors->first('title') }}</div>
                               @endif
                           </div>

                           <div class="form-group">
                               <label for="cover">{{ __('Cover') }}</label><br>
                               @if ($article->cover)
                                   <img width="75" height="75" class="rounded-circle" src="{{ Storage::url($article->cover) }}">
                               @endif
                               <input id="cover" class="form-control" type="file" name="cover" accept=".jpg, .jpeg">
                               @if($errors->has('cover'))
                                   <div class="alert-danger">{{ $errors->first('cover') }}</div>
                               @endif
                           </div>

                           <div class="form-group">
                               <label for="description">{{ __('Description') }}:</label>
                               <textarea id="description" class="form-control" name="description">{{ old('description', $article->description) }}</textarea>
                               @if($errors->has('description'))
                                   <div class="alert-danger">{{ $errors->first('description') }}</div>
                               @endif
                           </div>

                           <div class="form-group">
                               <label for="article_type">{{ __('Article type') }}</label>
                               <select class="form-control" id="article_type" name="article_type">
                                   @foreach($articleTypes as $articleType)
                                       <option value="{{ $articleType['id'] }}" {{ ($articleType['id'] == old('article_type', $article->article_type))? 'selected' : '' }}>{{ $articleType['name'] }} | {{ $articleType['description'] }}</option>
                                   @endforeach
                               </select>
                               @if($errors->has('article_type'))
                                   <div class="alert-danger">{{ $errors->first('article_type') }}</div>
                               @endif
                           </div>

                           <div class="form-group">
                               <label for="author_id">{{ __('Author') }}:</label>
                               <select class="form-control" name="author_id" id="author_id">
                                   <option value="" disabled>{{ __('Pasirinkite autorių') }}</option>
                                   @foreach($authors as $author)
                                       <option value="{{ $author->id }}" {{ ($author->id == old('author_id', $article->author->id))? 'selected' : '' }}>{{ $author->first_name . " " . $author->last_name }}</option>
                                   @endforeach
                               </select>
                               {{--<input id="author" class="form-control" type="text" name="author" value="{{ old('author', $article->author) }}">--}}
                               @if($errors->has('author_id'))
                                   <div class="alert-danger">{{ $errors->first('author_id') }}</div>
                               @endif
                           </div>

                           <div class="form-group">
                               <label for="slug">{{ __('Slug') }}:</label>
                               <input id="slug" class="form-control" type="text" name="slug" value="{{ old('slug', $article->slug) }}">
                               @if($errors->has('slug'))
                                   <div class="alert-danger">{{ $errors->first('slug') }}</div>
                               @endif
                           </div>
                           <div class="form-group">
                               <label>{{ __('Categories') }}:</label>
                               @foreach($categories as $category)
                                   <br>
                                   <label for="category_{{ $category->id }}">
                                       <input type="checkbox" name="category[]" id="category_{{ $category->id }}"
                                              value="{{ $category->id }}"
                                       {{ in_array($category->id, old('category', $article->categories->pluck('id')->toArray()))? 'checked' : '' }}> {{ __($category->title) }}
                                   </label>
                               @endforeach
                                @if($errors->has('category'))
                                   <div class="alert-danger">{{ $errors->first('category') }}</div>
                               @endif
                           </div>
                           <div class="form-group">
                               <input class="btn btn-success" type="submit" value="{{ __('Update') }}">
                           </div>

                       </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
