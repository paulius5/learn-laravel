@extends('layouts.front')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ __('Contact Us') }}
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif

                        <div class="row">

                            <div class="col-md-5">

                                <form action="" method="post">

                                    @csrf

                                    <div class="form-group">
                                        <label for="full_name">{{ __('Full name') }}:</label>
                                        <input id="full_name" class="form-control" type="text" name="full_name"
                                               value="{{ old('full_name') }}" required>
                                        @if($errors->has('full_name'))
                                            <div class="alert-danger">{{ $errors->first('full_name') }}</div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="email">{{ __('Your email') }}:</label>
                                        <input id="email" class="form-control" type="email" name="email"
                                               value="{{ old('email') }}" required>
                                        @if($errors->has('email'))
                                            <div class="alert-danger">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="content">{{ __('Message') }}:</label>
                                        <textarea id="content" class="form-control" name="content" rows="5"
                                                  required>{{ old('content') }}</textarea>
                                        @if($errors->has('content'))
                                            <div class="alert-danger">{{ $errors->first('content') }}</div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <input class="btn btn-success" type="submit" value="{{ __('Send') }}">
                                    </div>

                                </form>
                            </div>

                            <div class="col-md-7">
                                Address: ...
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection