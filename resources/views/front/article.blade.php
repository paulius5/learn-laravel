@extends('layouts.front')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">{{ $article->title }}</h3>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 text-sm-center text-md-left">
                                @if ($article->cover)
                                    <img class="img-fluid" src="{{ Storage::url($article->cover) }}">
                                @endif
                            </div>
                            <div class="col-md-6">
                                <p><em>{{ __('Author: ') }}<strong>{{ $article->author->first_name }} {{ $article->author->last_name }}</strong></em></p>
                                <p>{{ __('Categories: ') }}
                                    @foreach($article->categories as $category)
                                        <em><a href="{{route('front.category.articles', [$category->slug])}}">{{ $category->title }}</a></em>
                                    @endforeach
                                </p>
                                <p><em>{{ __('Created at: ') }} <strong>{{ $article->created_at->diffForHumans() }}</strong></em></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                {{ $article->description }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection