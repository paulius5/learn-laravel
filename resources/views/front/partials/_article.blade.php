<div class="col-md-6">
    @if ($article->cover)
        <img style="border-radius: 5px 100px;" class="pull-left pr-3 img-fluid" src="{{ Storage::url($article->cover) }}">
    @endif
    <h5><a href="{{ route('front.article.slug', [$article->slug]) }}">{{ $article->title }}</a></h5>
    <p>{{ __('Created by: ') }}
        <strong>{{ $article->author->first_name }}{{ $article->author->last_name }}</strong></p>
    <p>{{ __('Posted at: ') }} {{ $article->created_at }}</p>
    <p>{{ str_limit($article->description,100) }}</p>
</div>